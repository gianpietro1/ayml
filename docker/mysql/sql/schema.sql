USE `database`;

DROP TABLE IF EXISTS `institution`;

CREATE TABLE `institution` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `company_id` int(11) NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `subdomain` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` date NOT NULL,
  `state` tinyint(1) NOT NULL,
  `logo_logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_multiple` tinyint(1) NOT NULL,
  `color_main` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `color_secondary` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `color_tertiary` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `institution` */

insert  into `institution`(`id`,`company_id`,`url`,`subdomain`,`created_at`,`state`,`logo_logo`,`logo_multiple`,`color_main`,`color_secondary`,`color_tertiary`) values ('292c5b6a-6cd6-4d83-aaec-16b7665e8d8b',168805,'http://bt.dev4a.aptitus.com','bt','2018-04-19',1,'CompanyProfile-ulima.png',1,'#f0944b','#252525','#EEEEEE'),('292c5b6a-6cd6-4d83-aaec-16b7665e8daa',168208,'http://test.dev4a.aptitus.com','test','2018-03-29',1,'CompanyProfile-0196fdf0e26d5c6ef4d023340820e405.jpeg',1,'#00FF00','#0000FF','#FF0000'),('55330eb7c-9bca-4013-b1bd-5bd551c2b2c',168867,'http://bt.pre4a.aptitus.com','bt','2018-04-16',0,'CompanyProfile-0196fdf0e26d5c6ef4d023340820e405.jpeg',0,'#FF0000','#00FF00','#00FF00'),('5590eb7c-9bca-4013-b1bd-5bd551c2b2c2',168867,'http://bt.pre4a.aptitus.com','bt','2018-04-16',0,NULL,0,'#FF0000','#00FF00','#00FF00'),('5590eb7c-9bca-4013-b1bd-5bd551c2bc2f',168867,'http://bt.pre4a.aptitus.com','bt','2018-04-16',0,NULL,0,'#FF0000','#00FF00',''),('5q5330eb7c-9bca-4013-b1bd-5bd551c2b2',168867,'http://bt.pre4a.aptitus.com','bt','2018-04-16',0,NULL,0,'#FF0000','#00FF00','#00FF00'),('987qwe4e-1c1c-11e8-7r4u-0ed5f89f718b',168220,'http://upn.dev4a.aptitus.com','upn','2018-03-02',1,'CompanyProfile-002720a17e9584c5acf8f52afca36858.jpg',0,'#0000FF','#FF0000','#00FF00'),('b6604392-1c1c-11e8-accf-0ed5f89f718b',168260,'http://upc.dev4a.aptitus.com','upc','2018-02-27',1,'CompanyProfile-ulima.png',1,'#FF0000','#00FF00','#0000FF'),('b6604392-1c1c-11e8-accf-0ed5f89f718c',0,'https://ulima.dev4a.aptitus.com','ulima','2018-04-19',0,'CompanyProfile-ulima.png',1,'#EEEEEE','#f0944b','#252525');