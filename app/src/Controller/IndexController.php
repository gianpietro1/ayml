<?php

namespace App\Controller;

use Instagram\Api;
use Instagram\Storage\CacheManager;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class IndexController
 *
 * @package App\Controller
 * @author Pedro Pairazaman Silva <pedrotlx@gmail.com>
 * @copyright (c) 2018, Orbis
 */
class IndexController extends Controller
{
    const INSTAGRAM_USER = "alejandroymarialaura";
    /**
     * @Route("/", name="index")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        // Comentado por Gianpietro Lavado, por error similar a "https://github.com/pgrimaud/instagram-user-feed/issues/77"
        // $cache = new CacheManager('/app/var/cache');
        // $api = new Api($cache);

        // $api->setUserName(self::INSTAGRAM_USER);

        // $feed = $api->getFeed();

        // return $this->render('index/index.html.twig', array(
        //     'feed' => $feed->getMedias()
        // ));
        return $this->render('index/index.html.twig');
    }
}