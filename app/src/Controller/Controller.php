<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class Controller
 *
 * @package App\Controller
 * @author Pedro Pairazaman Silva <pedrotlx@gmail.com>
 * @copyright (c) 2018, Orbis
 */
class Controller extends AbstractController
{

}