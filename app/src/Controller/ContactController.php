<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Swift_Mailer;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactController
 *
 * @package App\Controller
 * @author Pedro Pairazaman Silva <pedrotlx@gmail.com>
 * @copyright (c) 2018, Orbis
 */
class ContactController extends Controller
{
    /**
     * @Route("/contacto", name="contacto", methods={"POST"})
     */

    public function indexAction(Request $request, Swift_Mailer $mailer)
    {
        $params = $request->request->all();

        $message = (new \Swift_Message('Tienes un nuevo seguidor'))
            ->setFrom('contacto@alejandroymarialaura.com')
            ->setTo("contacto@alejandroymarialaura.com")
            ->setBody(
                "Tienes un nuevo seguidor:<br />
                        nombre: ".$params['nombre']."<br />
                        email: ".$params['email']."<br />
                        ciudad: ".$params['ciudad'],
                'text/html'
            );

        $mailer->send($message);

        return new JsonResponse([
            'message' => 'Enviado!'
        ]);
    }
}